import { useContext } from 'react';
import CourseCard from '../../components/CourseCard';
import { Table, Button } from 'react-bootstrap';
import coursesData from '../../data/courses';
import UserContext from '../../UserContext';
import Head from 'next/head';//lets acquire the needed components and packages in order to build our courses page. 

//lets create a function that return our courses catalog/page. 
export default function index(){
	//use the UserContext and we destructured it to access the user that we will define in the App component. 
	const { user } = useContext(UserContext)

	//lets create a component for each our courses in our records.
	const courses = coursesData.map(course => {
	//lets create a control structure that will determine if the course is still available for enrollment. 
	if(course.onOffer){
			return (
				<CourseCard key={course.id} course={course}/>
				);
		}else {
			return null; 
		}
	})
		
	//lets create rows to be rendered in a bootstrap table when an admin is logged in, in our courses catalog this will also have an admin panel section 
	const coursesRows = coursesData.map(course => {
		return (
               <tr key={course.id}>
                   <td>{course.id}</td>
                   <td>{course.name}</td>
                   <td>{course.price}</td>
                   <td>{course.onOffer ? 'open' : 'closed'}</td>
                   <td>{course.start_date}</td>
                   <td>{course.end_date}</td>
                   <td>
                        <Button variant="warning">Update </Button>                 
                       <Button variant="danger"> Disable </Button>                      	
                   </td>               
               </tr>
			);
	})
    //lets create a ternary that will return the proper display depending on the user type. 
		return (
            user.isAdmin === true 
            ? 
            <>
	            <Head>
	               <title>Courses Admin Dashboard</title>
	            </Head>
	            <h1>Course Dashboard</h1>
                <Table striped bordered hover>
                    <thead>
                       <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Actions</th>                       
                       </tr>
                    </thead> 
                    <tbody>
                    	{ coursesRows }
                    </tbody>               	
                </Table>	            
	        </> 
            :
            <>
             <Head>
                 <title>Courses Index</title>
             </Head>
             { courses } 
            </>		
		)
}
