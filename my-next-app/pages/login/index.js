import { useState, useContext, useEffect } from 'react';
import UserContext from '../../UserContext';
import users from '../../data/users';
import Router from 'next/router';
import Head from 'next/head'
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import View from '../../components/View';
import { GoogleLogin } from 'react-google-login'
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2'

export default function index(){
    //use the UserContext and lets destructure it to access the user and setUser that we will define in the app component. 
    const { user, setUser } = useContext(UserContext)

    //lets create a state hook for our login page
    //states for form input 
    const [ email, setEmail] = useState('');
    const [ password, setPassword] = useState('');
    const [tokenId, setTokenId] = useState(null)//lets create first a state hook for our token
    
    const authenticateGoogleToken = (response) => {
        console.log(response) //we are trying to catch the response from google. 
        const payload = {
                method: 'post',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ tokenId: response.tokenId })
        }
         fetch (`${ AppHelper.API_URL }/users/verify-google-id-token`, payload)
            .then(AppHelper.toJSON)
            .then(data => {
                if (typeof data.accessToken !== 'undefined'){
                    localStorage.setItem('token', data.accessToken)
                    retrieveUserDetails(data.accessToken)
                } else {
                    if (data.error == 'google-auth-error'){
                        Swal.fire(
                            'Google Auth Error',
                            'Google authentication procedure failed',
                            'error'
                        )
                    } else if (data.error === 'login-type-error') {
                        Swal.fire(
                            'Login Type Error',
                            'You may have registered through a different login procedure',
                                'error'
                        )
                    }
                }
            })
    } 

    //lets create a function that will return the Login form for our user
    const LoginForm = () => {
        return(
             <Card>
                    <Card.Header>Login Details</Card.Header>
                    <Card.Body>
                        <Form onSubmit={ authenticate }>
                            <Form.Group controlId="userEmail">
                                <Form.Label>Email Address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    value={ email } 
                                    onChange={ (e) => setEmail(e.target.value) } 
                                    autoComplete="off" 
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    value={ password } 
                                    onChange={ (e) => setPassword(e.target.value) } 
                                    required
                                />
                            </Form.Group>
                            <Button className="mb-2" className="bg-success" type="submit" block>Login</Button>
                            <GoogleLogin
                                clientId="1079432757411-6if41u500p48b8rd1rdig8mj718htuh0.apps.googleusercontent.com"
                                buttonText="Login"
                                onSuccess={ authenticateGoogleToken }
                                onFailure={ authenticateGoogleToken }
                                cookiePolicy={ 'single_host_origin' }
                                className="w-100 text-center d-flex justify-content-center"
                            />
                        </Form>
                    </Card.Body>
                </Card>
            )
    }

    //lets create a function to retrieve the userDetails 
    const retrieveUserDetails = (accessToken) => {
        //we are going to create an object which we will name as option
        const options = {
            headers: { Authorization: `Bearer ${accessToken}`}
        }
        //lets create a fetch request 
        fetch(`${AppHelper.API_URL}/users/details`, options).then(AppHelper.toJSON).then(data => {
            setUser({ id: data._id, isAdmin: data.isAdmin})
            Router.push('/courses')//we are targetting the state setter for the user to be stored in the user context object. THEN redirect the user to courses page. 
        })
    }

    //in order to test our authentication, lets use an effect hook that will run whenever any of the specified user details have been changed. 
    useEffect(() => {
        console.log(`User with email: ${user.email} is an admin: ${user.isAdmin}`); 
    }, [user.isAdmin, user.email]); 

    //lets create a function that will simulate a login page/user authentication. 
    function authenticate(e){
        e.preventDefault() //prevent redirection via form submission
        //authentication based on imported users data
        //instead of a "simulation" for a user authentication we are now going to modify this method to be an actual fetch requet to save the gmail user inside the database. 
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'  },
            body: JSON.stringify({ email: email, password: password })
        }
        //lets wrap/inject the behavior inside a control structure so that we will be able to identify the responses in their specific condistions
        fetch(`${ AppHelper.API_URL }/users/login`, options).then(AppHelper.toJSON).then(data => {
            if(typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
               //inside this branch we are going to give it these exact conditions upon failure
               if(data.error === 'does-not-exist'){
                 Swal.fire('Authentication Failed', 'User does not exist', 'error')
               } else if(data.error === 'incorrect-password') {
                 Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
               } else if(data.error === 'login-type-error') {
                  Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try using an alternative login procedure', 'error')
               }            
            }
        })
    }
    return(
            <View title={'Login'}>
                <Row className="justify-content-center">
                    <Col xs md="6">
                        <h3>Login</h3>
                        <LoginForm />
                    </Col>
                </Row>
            </View>   
        )
}
