import { useState, useEffect } from 'react'; 
import Head from 'next/head';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'//lets acquire the basic components needed in order to build a register page 
//modify all the alert function in our register page and send a video that shows the change behavior of the pop up boxes whenever you try to register a new user. 
//output: 3 photos in 1 screenshots

export default function index(){
     //lets apply a state hook for our form input fields
     const [email, setEmail]= useState('')
     const [password1, setPassword1]= useState('')
     const [password2, setPassword2]= useState('')  

     //lets create another state to determine wether the submit button is enabled or not. 
     const [isActive, setIsActive ] = useState(false)

     //lets create a validation to validate the inputs inside the form
     useEffect(() => {
       //lets create a condition/validation to enable the submit button when all the fields are polulated. 
       //1. email, password1, and pass2 is not blank
       //2. password2 should match password1
       if((email !== '' && password1 !== '' && password2 !== '') && (password2 === password1)){
            setIsActive(true)
       }else{
          setIsActive(false)
       }
     }, [email, password1, password2])//for cleanup 

     //lets create a function that will simulate a user registration
     function registerUser(e){
        e.preventDefault()
        //lets clear out the input fields
        setEmail(''); 
        setPassword1('');
        setPassword2('');
        alert('Successfully Registered')
        console.log('Thank you for registering'); 
     }
  return(
        <>
           <Head>
               <title> Register Here </title>
           </Head>
           <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label> Email Address </Form.Label>
                    <Form.Control type="email" placeholder="Enter Email Here" value={email} onChange={e => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="password1">
                    <Form.Label> Password </Form.Label>
                    <Form.Control type="password" placeholder="Enter Password Here" value={password1} onChange={e => setPassword1(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="password2">
                    <Form.Label> Confirm Password </Form.Label>
                    <Form.Control type="password" placeholder="Confirm Password Here" value={password2} onChange={e => setPassword2(e.target.value)} required/>
                </Form.Group>
                {isActive ?
                   <Button variant="success" type="submit" id="submitBtn">Register</Button>
                  :
                  <Button variant="success" type="submit" id="submitBtn" disabled>Register</Button>   
                }
                </Form> 
                
      </> 
    )
} 
