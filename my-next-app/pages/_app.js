import { useState, useEffect } from 'react'; 
import { Container } from 'react-bootstrap'; 
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from '../components/Navbar'; 
import { UserProvider } from '../UserContext'; 

export default function MyApp({ Component, pageProps }) {
	
	const [user, setUser] = useState({
		email: null, 
		isAdmin: null
	})
		useEffect(() => {
		setUser({
			email: localStorage.getItem('emai'), 
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})
	}, [])
	useEffect(()=> {
		console.log(user.email);
	}, [user.email])

	const unsetUser = () => {
		localStorage.clear()
		
		setUser({
			email: null, 
	 		isAdmin: null 
		});
	}
    return(
    	<>
                <UserProvider value={{user, setUser, unsetUser}}>
	                <Navbar />
			        <Container> 
			    	<Component {...pageProps} />
			        </Container>    
	            </UserProvider>   
        </>    	
    	);
}
