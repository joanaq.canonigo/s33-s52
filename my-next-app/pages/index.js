import Head from 'next/head';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Navbar from '../components/NavBar'; 

export default function Home(){
	const data = {
		title: "Batch 68 First Next App", 
		content: "Semi Cute Kami", 
		destination: "/courses", 
		label: "Enroll Now!"
	}
	return(
        <> 
           <Banner dataProp={data}/>
           <Highlights/>	
        </>           	  
		)
}
