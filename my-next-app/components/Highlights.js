import { Row, Col } from 'react-bootstrap'; //lets acquire the bootstrap grid system 
//lets also acquire the Card component from bootstrap 
import Card from 'react-bootstrap/Card'; 

//lets create a Highlights() to build our Highlights component 
export default function Highlights() {
  return(
        <Row>
           <Col>
               <Card className="cardHighlight">
                   <Card.Body>
                       <Card.Title>
                           <h2>Learn from Home</h2>
                       </Card.Title>
                       <Card.Text>
                            Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Itaque rerum ullam expedita laudantium, dicta et error magnam molestiae fugit nulla natus earum sunt dolor hic quae dolorum ducimus fugiat. Magni!
                       </Card.Text>
                   </Card.Body>
               </Card>           
           </Col>
           <Col>
               <Card className="cardHighlight">
                   <Card.Body>
                       <Card.Title>
                           <h2>Study Now, Pay Later</h2>
                       </Card.Title>
                       <Card.Text>
                            Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Itaque rerum ullam expedita laudantium, dicta et error magnam molestiae fugit nulla natus earum sunt dolor hic quae dolorum ducimus fugiat. Magni!
                       </Card.Text>
                   </Card.Body>
               </Card>           
           </Col>
           <Col>
               <Card className="cardHighlight">
                   <Card.Body>
                       <Card.Title>
                           <h2>Be Part of Our Community</h2>
                       </Card.Title>
                       <Card.Text>
                            Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Itaque rerum ullam expedita laudantium, dicta et error magnam molestiae fugit nulla natus earum sunt dolor hic quae dolorum ducimus fugiat. Magni!
                       </Card.Text>
                   </Card.Body>
               </Card>           
           </Col>
        </Row>
    )
}
