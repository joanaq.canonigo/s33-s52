import { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar'; 
import Nav from 'react-bootstrap/Nav'; 
import Link from 'next/link';
import UserContext from '../UserContext'; 

export default function NavBar(){
  
  const { user } = useContext(UserContext); 
  return (
          <Navbar bg="light" expand="lg">
          <Link href="/">
             <a className="navbar-brand">Next-Booking-App</a>
          </Link>  
          <Navbar.Toggle aria-controls="basic-navbar-nav"/>  
          <Navbar.Collapse id="basic-navbar-nav">
           <Link href="/courses">
                   <a className="nav-link"> Courses </a>            
               </Link>  

               {(user.email !== null)
                 ? (user.isAdmin === true )
                  ? 
                  <>
                     <Link href="/logout">
                        <a className="nav-link" role="button">Logout </a>             
                     </Link>  
                     <Link href="#">
                        <a className="nav-link" role="button">Add Course</a>             
                     </Link>   
                  </> 
                  :
                  <>
                    <Link href="/logout">
                        <a className="nav-link" role="button">Logout </a>             
                     </Link> 
                  </>   
                  :
                  <>
                    <Link href="/register">
                        <a className="nav-link" role="button">Register</a>
                    </Link> 
                    <Link href="/login">
                        <a className="nav-link" role="button"> Login </a>          
                    </Link> 
                  </> 
               }
          
          </Navbar.Collapse>         
          </Navbar>   
    )
}
