import { Jumbotron, Row, Col } from 'react-bootstrap';
import Link from 'next/link';//lets acquire a third component from next 
import PropTypes from 'prop-types'//this will serve as a checker to see if the components are recieving trhe correct data types. 

//lets create a function that will allow us to create a Banner component
export default function Banner({dataProp}) {
  //lets now destructure the data prop into its properties 
  const { title, content, destination, label } = dataProp; 
  return (
        <Row>
           <Col>
              <Jumbotron>
                  <h1> { title }</h1>
                  <p> { content }</p>
                  <Link href={destination}>
                      <a>{label}</a>
                  </Link>
              </Jumbotron>
           </Col>
        </Row>
    )
} 

//lets use the propTypes to check if the banner is getting the correct kind of data. 
Banner.propTypes = {
    //we are going to use the shape() to check that the prop objects conforms to a specific shape. 
    data: PropTypes.shape({
      //we will define the properties of our object and their expected data types. 
       title: PropTypes.string.isRequired, 
       content: PropTypes.string.isRequired, 
       destination: PropTypes.string.isRequired,
       label: PropTypes.string.isRequired
    })
}
