import  { useState, useEffect } from 'react'; 
import { Card, Button } from 'react-bootstrap'; 
import PropTypes from 'prop-types'; 
import AppHelper from '../app-helper';//lets acquire first the apphelper module
//lets create a state hook for our course component: useState()

export default function CourseCard({ course }){
    const { name, description, price, start_date, end_date } = course

  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(20); 
  const [isOpen, setIsOpen] = useState(true); 

  function enroll(courseId){ 
        fetch(`${ AppHelper.API_URL }/users/enroll`, {
          method: 'POST', 
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            courseId: courseId 
          })
        })
        .then(res => {
          return res.json()
        })
        .then(data => {
          console.log(data)
        })
  } 
 useEffect(() => {
       if(seats === 0){
          setIsOpen(false);  
      }
    }, [seats])
  return(
          <Card>
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Text>         
          <span className="subtitle"> Description:  </span>
          {description}
          <br />
          <span className="subtitle"> Price:  </span>
          PHP {price}
          <br />
          <span className="subtitle"> Start Date:   </span>
          {start_date}
          <br />
          <span className="subtitle"> End Date : </span>
          {end_date}
          <br />
              </Card.Text>
              {isOpen ?
                <Button className="bg-primary" onClick={() => {enroll(course._id)}}>Enroll</Button>
                : 
                <Button className="bg-primary" disabled>Not Available</Button>
              }                         
            </Card.Body> 
          </Card>   
    )
}
CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}
