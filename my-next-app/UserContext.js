import React from 'react';//we are going to create a context data for our app 

//1. Lets create a Context Object and we will place it inside a variable called UserContext
const UserContext = React.createContext();

//2. Lets acquire its Provider component which will allow us to consume components subscribed to context changes
export const UserProvider = UserContext.Provider; 

export default UserContext;
