const Course = require('../models/course') //we required the model of the course

//lets create a function that will allow us to display or get all the courses from our database. 
module.exports.getAll = () => {
	return Course.find({ isActive: true }).then(courses => courses)
} 

//lets create a function that will allow us to create/add a new course inside the databse. 
module.exports.add = (params) => {
   //lets declare a variable that will allow us to insert the data created by the user
   let course = new Course({
   	   name: params.name,
   	   description: params.description,
   	   price: params.price
   })
   //now that we were able to get the data inserted by the user and we placed it inside a variable we now have to save the data to create a new course in our system. 
   return course.save().then((course, err) => {
   	   return (err) ? false : true
   })
} 

//lets now create a function for us to be able to find a specific course from our database.
//upon searching for a specific course the user must first identify the course he/she is trying to look for  
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course)
}

//lets now create an update function for our our courses
//this update method requires input from the user
module.exports.update = (params) => {
	const updates = {
		name: params.name,
		description: params.description,
		price: params.price	
	}
	//once we got the params or data that the user will input its to identify or locate the course that we want to update
	// this function will not only allow the the application to locate the requested object/document but it will also allow us to update the necessary fields
	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true 
	})
}

//why is our controller for course is not yet done? ..lets go back to our course model ..and see why? 
//why do we have to create a seperate update function between the (name, description, and price properties) with the (isACTIVE) property?
//this new function will be used so that an admin can change the status of a course from being open to close and vice versa
module.exports.archive = (params) => {
	//lets declare a variable that will identify the property of the document that we would like to change and give it our desired value
	const updates = { isActive: false }
	//we now have to properly "identify and locate" the Course that we would like to update
	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}
