const User = require('../models/user')
const Course = require('../models/course')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const sms = require('../sms')

const { OAuth2Client } = require('google-auth-library')

const clientId = '1079432757411-6if41u500p48b8rd1rdig8mj718htuh0.apps.googleusercontent.com'


module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),
		loginType: 'email'
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})
}

module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) { return false }
        if (user.loginType !== 'email') { 
		       return { error: 'login-type-error' }
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}
	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

module.exports.enroll = (params) => {
	console.log(params)
	return User.findById(params.userId).then(user => {
		user.enrollments.push({ courseId: params.courseId })

		return user.save().then((user, err) => {
			return Course.findById(params.courseId).then(course => {
				course.enrollees.push({ userId: params.userId })

				return course.save().then((course, err) => {
		                    if (err) {
		                        return false
		                    } else {
		                        console.log(user.mobileNumber)
		                        sms.send(user.mobileNumber, `Good day! At ${ new Date().toLocaleString() }, you have enrolled on a course entitled ${ course.name }. Enjoy the course and learn continuously!`)
		                        return true
		                    }
				})
			})
		})
	})
}

module.exports.updateDetails = (params) => {
	
}

module.exports.changePassword = (params) => {
	
}

module.exports.verifyGoogleTokenId = async (tokenId) => {
   const client = new OAuth2Client(clientId);//we are going to instantiate a new OAuthClient and place it inside a variable named client
   const data = await client.verifyIdToken({
   	  idToken: tokenId,
   	  audience: clientId  //this is to specify kung sino yung nag rerequest ng verification na ID token. 
   })
   //lets create now a control structure that will determine if email has been verified. 
   if(data.payload.email_verified === true){
     const user = await User.findOne({ email: data.payload.email }).exec()//the exec method will be used to check for a match string
     if(user !== null){
     	//lets create another sub condition that will rely upon the login type of the user
     	if(user.loginType === 'google'){
     		return { accessToken: auth.createAccessToken(user.toObject()) };
     	}else {
     		return { error: "login-type-error"}; 
     	}
   }else {
   	  //if the user is not yet existing in our records then we will have to register then a new user.
   	  const newUser = new User({
   	  	firstName: data.payload.given_name,
   	  	lastName: data.payload.family_name, 
   	  	email: data.payload.email, 
   	  	loginType: "google"
   	  }); 

   	  return newUser.save().then((user, err) => {
   	  		return { accessToken: auth.createAccessToken(user.toObject()) }; 
   	  }); 
   	}
   } else {
   	  return { error: "google-auth-error" }; 
   }
}; 
