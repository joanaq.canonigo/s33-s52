const mongoose = require('mongoose')

const courseSchema = new mongoose.Schema({

	name: {
		type: String, 
		required: [true, 'Course name is required']
	},
	description: { //to describe the course for the clients
		type: String,
		required: [true, 'Course Description is required']
	},
	price: { //this will describe how much the course cost
		type: Number,
		required: [true, 'Course price is required']
	}, 
	isActive : { //this will describe if the course is still available for enrollment
 		type: Boolean, 
 		default: true 
	},
	createdOn : { //this will describe the date when the course was created in our database
		type: Date, 
		default: new Date()
	}, 
	enrollees: [
      {
      	userId: { //this describes the userId of the student
      		type: String, 
      		required: [true, 'userId is required']
      	},
      	enrolledOn: { //this describes the date when the student enrolled for the class. 
      		type: Date,
      		default: new Date()
      	}
      }
	]
})

module.exports = mongoose.model('course', courseSchema)
