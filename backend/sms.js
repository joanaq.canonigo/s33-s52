const request = require('request')

		module.exports.send = (mobileNumber, message) => {
		    const shortCode = '225657209 '
		    const accessToken = '"aY2zF2VbU3h5TimbbBjWM87fi4en_ovXYM-0-129qq0'
		    const clientCorrelator = '123576'

		    const options = {
		        method: 'POST',
		        url: 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/' + shortCode + '/requests',
		        qs: { 'access_token': accessToken },
		        headers: { 'Content-Type': 'application/json' },
		        body: {
		            'outboundSMSMessageRequest': { 
		                'clientCorrelator': clientCorrelator,
		                'senderAddress': shortCode,
		                'outboundSMSTextMessage': { 'message': message },
		                'address': mobileNumber 
		            } 
		        },
		        json: true 
		    }

		    request(options, (error, response, body) => {
		        if (error) throw new Error(error)
		        console.log(body)
		    })
		}